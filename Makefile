FILE := main
OUT  := build

pdf:
	latexmk -outdir=$(OUT) -pdf $(FILE)

clean:
	rm $(filter-out $(OUT)/$(FILE).pdf, $(wildcard $(OUT)/*))

purge:
	rm -rf $(OUT)

.PHONY: latexmk clean purge
