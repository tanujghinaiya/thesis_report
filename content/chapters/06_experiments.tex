\chapter{Experiments} \label{chapter:experiments}
In the last chapter, we created the two datasets (see \cref{section:dataset_creation}) needed for training of the deep learning models. The first one ``raw'', has been created entirely from crowdsourced data from OSM \cite{osm} which was inferred into power lines by the Transnet project \cite{transnet}. The second dataset has been created by filtering and correcting annotations of approximately half of the ``raw'' dataset.

In this chapter, we focus on the experimentation leading up to the final model that is presented in this thesis. In addition, the results of the experiments are also discussed. The experiments are conducted in a manner by which the solution converges to the final model and the final set of parameters in a systematic manner.

The first section discusses various feature sets and their performance for the task of tower detection. In the second section, the effect of correction of location data is studied. The third section discusses the training performed on the partial ``corrected'' dataset on both the models. Additionally, both the models are fine-tuned using the complete ``corrected'' dataset and the results of the training are discussed.

\section{Feature Selection} \label{section:feature_selection}
    Feature selection is a very important process in machine learning which is used to select the best feature set to be used for the training and evaluation of a model. In addition, it also aids in reduction of dimensionality and training times while maximizing the prediction performance \cite{introfeaturesel,featureselphd}. For example, Chen et. al. \cite{hdnn} have used a combination images thresholded at various levels and HOG \cite{hog} for each of the thresholded image. On the other hand, CNNs have proven to be very effective as feature extractors and often have outperformed hand crafted features based models as shown by Antipov et. al. \cite{learnedvshandcrafted}. Considering that effectiveness of machine learning models varies with data and the application domain \cite{mlusefulthings}, a study is done to compare different feature sets for this task.

    The CNN3 (see \cref{subsection:cnn3}) model has been used to evaluate the feature sets. The learning rate for the training has been set to $0.001$. $L1$, $L2$ and dropout regularization have been set to $0$. The partial ``raw'' dataset which consists of $5,000$ positive samples and $10,000$ negative samples has been used for the purposes of training the model. The input image resolution is $140 \times 140$ pixels. The batch size used for training is $32$. The input features being tested are ``RGB'', ``Grayscale'', ``Thresholding + Dilation + Erosion'' and ``Edges + HOG + Grayscale'' (see \cref{section:input_features}).

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/feature_selection_loss}
            \caption{Validation loss of various feature sets.}
            \label{fig:feature_comparision_loss}
        \end{subfigure}
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/feature_selection_acc}
            \caption{Binary accuracy of various feature sets.}
            \label{fig:feature_comparision_acc}
        \end{subfigure}
        \caption{Comparison of performance of various feature sets.}
        \label{fig:feature_comparision}
    \end{figure}

    The results of the experiments are shown in Figure~\ref{fig:feature_comparision}. The grayscale imagery performed the worst out of all. It was characterized by low gradients across the training and the binary accuracy of the predictions remained quite low. The ``Edges + HOG + Grayscale'' imagery performed marginally better with a loss of around 0.44 and a binary accuracy of around $77\%$ after 100 epochs. The ``Thresholding + Dilation + Erosion'' feature set which is a reduced feature set because of its $140 \times 140 \times 1$ input shape, performed the second best. It was quick to optimize and started saturating after $60$ epochs. However, the optimization became unstable and it crashed at epoch $68$. The highest binary accuracy achieved by it was about $84\%$. RGB imagery performed the best amongst all four feature sets. It was slower to optimize than all but the grayscale imagery, however, the saturation did not begin until epoch $90$ after which the gradients were not that big. The accuracy after $100$ epochs was around $90\%$ with a loss of around $0.28$.

\section{Effect of Data Correction} \label{section:correction_effect}
    Since the data sourced from the Transnet project \cite{transnet} has been obtained from crowdsourcing, it has considerable noise which may impede the process of learning. This is because the model would go on to learn the noise in the data leading to overfitting. The enrichment of data was proposed in which the bad quality images would be removed followed by manual correction of the location data. This was performed by deletion of $3,223$ images followed by correction of $11,300$ samples in $11,186$ images.

    As before, the CNN3 model has been used to evaluate the datasets. A higher learning rate of $0.005$ was chosen. $L1$ and $L2$ regularization were set at $0.00001$ and a dropout rate of $0.5$ was selected. A batch size of $32$ was used for training. Both the ``raw'' and ``corrected'' datasets have $5,000$ positive samples and $10,000$ negative samples.

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/correction_loss}
            \caption{Validation loss of raw and corrected datasets.}
            \label{fig:correction_loss}
        \end{subfigure}
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/correction_acc}
            \caption{Binary accuracy of raw and composed datasets.}
            \label{fig:correction_acc}
        \end{subfigure}
        \caption{Comparison of performance of raw and corrected datasets.}
        \label{fig:correction_comparision}
    \end{figure}

    Figure~\ref{fig:correction_comparision} shows the results of training on both the raw and the corrected datasets. The increment in performance is substantial over the raw dataset. Additionally, the higher performance is consistent across the complete experiment. The model training on the raw dataset starts to overfit at around epoch $50$ recognizable by the increase in the validation loss. On the other hand, the corrected dataset continues to converge until the end of the experiment at epoch $100$. Also, the binary accuracy of the model using the corrected dataset is at about $97\%$, $5\%$ more than the one with the raw dataset. Considering these observations, we can conclusively say that the correction of the dataset improves the model both in terms of performance and overfitting.

\section{Training of the Deep Learning Models using the Corrected Dataset}
    In the previous experiments, we observed that the RGB imagery provides better performance. In addition, we observed a considerable performance increase because of correction of the dataset. Now, we shall train both the deep learning models with the partial corrected datasets. This is done so that we can pre-train the models while keeping the training time to a minimum.

    \begin{figure}[htbp]
        \centering
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/training_loss}
            \caption{Validation loss of CNN3 and HDNN.}
            \label{fig:training_loss}
        \end{subfigure}
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/training_acc}
            \caption{Binary accuracy of CNN3 and HDNN.}
            \label{fig:training_acc}
        \end{subfigure}
        \caption{Training of models using partial corrected dataset.}
        \label{fig:training}
    \end{figure}

    Figure~\ref{fig:training} shows the validation loss and accuracy of the two deep learning models implemented in this thesis. The performance of both the models seems to be near equivalent. The loss value is a bit less than $0.1$ and the accuracy is around $98\%$. Now, we perform additional training using the complete dataset. This will fine tune the network further and would also make the models more resilient to noise. The learning rate for CNN3 is set to $0.005$ with a decay of $0.003$. The learning rate for the HDNN model is kept at $0.005$. The $L1$ and $L2$ are kept at $0.00001$ as before and a dropout of $0.5$ is applied.

    The results of the fine tuning are shown in Figure~\ref{fig:finetuning}. From the results, there is no superior model out of the two. The HDNN has a much lower loss, while the CNN3 has a much higher binary accuracy. After epoch 80, the CNN3 overfits heavily. Thus, as the final model, the weights from the position of the lowest loss, are saved to the disc. Further evaluation will be performed in the next chapter.

    \begin{figure}[H]
        \centering
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/finetuning_loss}
            \caption{Validation loss of CNN3 and HDNN.}
            \label{fig:finetuning_loss}
        \end{subfigure}
        \begin{subfigure}{0.475\textwidth}
            \centering
            \includegraphics[width=\textwidth]{content/assets/figures/finetuning_acc}
            \caption{Binary accuracy of CNN3 and HDNN.}
            \label{fig:finetuning_acc}
        \end{subfigure}
        \caption{Finetuning of models using the complete corrected dataset.}
        \label{fig:finetuning}
    \end{figure}
