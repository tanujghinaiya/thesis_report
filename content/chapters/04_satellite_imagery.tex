\chapter{Satellite Imagery}\label{chapter:satellite_imagery}
    Remote sensing refers to the collection of data about a phenomenon of interest from a distance without any direct interaction with the phenomenon \cite{geog260,introremotesensing}. Humans can sense their environment using their noses, ears and eyes, thus, making them natural remote sensors. Similarly, satellites are used to observe the earth by collecting electromagnetic radiation (or specific bands of the electromagnetic spectrum, for e.g. the visible spectrum). The pictorial representation \cite{introtoremotesensing} of this data is referred to as \emph{satellite imagery}.

    Satellite imagery is used in applications in a variety of domains. For example, monitoring of plant health and foliage change using Landsat imagery. Another example is the use of IKONOS \cite{ikonos} imagery to analyse urban infrastructure.

    \begin{table}[htbp]
    \centering
    \resizebox{\textwidth}{!}{%
    \begin{tabular}{@{}llllll@{}}
    \toprule
    Type                               & Bands & MS (m) & Pan (m) & Scene size (km) & Cost (per sq. km) \\
    \midrule
    CORONA \cite{corona}               & 1     & —      & 2-8     & 17 x 232        & \$30 per scene    \\
    Landsat 4-5 MSS \cite{landsat:mss} & 4     & 80     & —       & 170 x 185       & Free              \\
    Landsat 4-5 TM \cite{landsat:tm}   & 7     & 30     & —       & 170 x 185       & Free              \\
    Landsat 7 ETM+ \cite{landsat:tmp}  & 8     & 30     & 15      & 170 x 185       & Free              \\
    Landsat 8 \cite{landsat:8}         & 11    & 30     & 15      & 170 x 185       & Free              \\
    SPOT 1-3                           & 3     & 20     & 10      & 60 x 60         & \$1,200 per scene \\
    SPOT 4                             & 4     & 20     & 10      & 60 x 60         & \$1,200 per scene \\
    SPOT 5                             & 4     & 10     & 2.5 / 5 & 60 x 60         & \$2,700 per scene \\
    SPOT 6-7 \cite{spot:67}            & 4     & 6      & 1.5     & 60              & \$5.15            \\
    IKONOS \cite{ikonos}               & 4     & 4      & 1       & 11.3            & \$10              \\
    QuickBird \cite{quickbird}         & 4     & 2.4    & 0.6     & 16.8            & \$16              \\
    Pléiades 1A-1B                     & 4     & 2      & 0.5     & 20              & \$13              \\
    WorldView-1                        & 1     & —      & 0.46    & 17.6 x 14       & \$13              \\
    WorldView-2 \cite{worldview2}      & 8     & 1.84   & 0.46    & 16.4            & \$29              \\
    WorldView-3                        & 28    & 1.24   & 0.31    & 13.1            & \$32              \\
    WorldView-4 \cite{worldview4}      & 4     & 1.24   & 0.31    & ?               & ?                 \\ \bottomrule
    \end{tabular}%
    }
    \caption{Comparision of specifications of various satellites, Source: ``Satellite Imagery: Types, Resolution, and Pricing'' by Rebecca Seifried \cite{satimagerytypes}}
    \label{table:satellite_comparision}
    \end{table}

    \section{Types of Satellite Imagery}
        \subsection{Panchromatic}
            Panchromatic refers to black and white images acquired by imaging sensors mounted on remote sensing platforms. The image captured corresponds to a wide range of the electromagnetic spectrum (usually visible spectrum) that is sensed by the sensor. This sensitivity to a wider spectrum allows the sensor to capture images at much higher resolutions than multispectral imagery since the energy required to ``fill'' the sensor is much less.

        \subsection{Multispectral}
            RGB is the de facto format for most images. It is composed of three channels of information, corresponding to red (R), green (G) and blue (B) levels in the image, essentially capturing the visible spectrum. In addition to the RGB channels, satellites have been capturing additional wavelengths beyond the visible spectrum since the 1980s \cite{satimagerytypes}. Such imagery is known as \emph{multispectral imagery}. For example, the GeoEye-1 \cite{geoeye1} satellite captures multispectral imagery with four bands (RGBN). Here N stands for near infrared imagery. Similarly Landsat 4 \& 5 \cite{landsat:bands} satellites acquire eight band multispectral imagery comprising of red, blue, green, near infrared, shortwave infrared 1, shortwave infrared 2 and thermal channels. The additional invisible spectrum acquired by multispectral imagery are essential in applications like vegetation detection and analysis, identification of subterranean archaeological features.

            Additionally, multispectral imagery can be used in conjunction with panchromatic imagery to create high resolution multichannel imagery, by merging the colour bands with high panchromatic imagery. This process is known as \emph{pan-sharpening}. Mapping services such as Google Maps, ArcGIS Online, Bing Maps etc. apply this technique to create high resolution satellite imagery that is offered by them. One thing to note is that these services only include the RGB channels as part of their services, omitting all the other bands available from the original satellite imagery.

        \subsection{Hyperspectral}
            Hyperspectral imagery refers to imagery acquired to cover entire spectrum of light in a continuous fashion rather than in discreet bands. This is achieved by acquiring imagery using numerous bands which when merged form a continuous band for the entire spectrum of light. Some applications of hyperspectral imagery include mapping of non-native plants \cite{nonnativeplants}, exploration of mineral diversity \cite{mineraldiversity} etc.

    \section{Resolution}
        The resolution of the imagery data acquired in context of satellite imagery can be broadly divided into four types based on the distinguishable characteristics of measurement.

        \subsection{Spectral Resolution}
            Spectral resolution refers to the sensitivity of the sensor towards detection of small variations in wavelength. For example, in a panchromatic image, an object of different colours would be indistinguishable if the objects reflects/emits the same amount of energy. A higher spectral resolution would make it easier to distinguish objects of different colours with the same intensity since it would better at distinguishing light of different wavelengths.

        \subsection{Spatial Resolution}  \label{subsection:spatial_res}
            Spatial resolution refers to the fineness of the raster grid formed by the acquired image. An image of higher resolution would correspond to a smaller area (in square meter) on ground covered by a pixel in the image. Spatial resolution is classified into the following categories based on the edge length of a pixel on ground.

            \begin{itemize}
                \item \emph{Low Resolution}: Larger than 30m
                \item \emph{Medium Resolution}: 2m to 30m
                \item \emph{High Resolution}: Less than 2m
            \end{itemize}

            Imagery with higher spatial resolution would contain more detail in it, however, it also introduces cost in terms of storage and processing.

        \subsection{Radiometric Resolution}
            Radiometric resolution refers to the sensitivity of the sensor towards small differences in the intensity of radiation for a pixel. A higher bit depth of the image corresponds to a higher radiometric resolution in satellite imagery. For example, an AVHRR (Advanced Very High Resolution Radiometer) \cite{avhhr} sensor has a bit depth of 10 with a spatial resolution of $\backsim4km$. In comparison, Landsat sensors \cite{landsat:tm,landsat:mss,landsat:tmp} record imagery with a bit depth of 8 with a spatial resolution ranging from 15m to 60m. However, imagery from AVHRR has a higher radiometric resolution despite its coarse spatial resolution.

        \subsection{Temporal Resolution}
            Temporal resolution refers to the time taken by the satellite to revisit a location at the same viewing angle. Temporal resolution of a satellite is determined by swatch overlap, latitude of the given location and the sensor's capabilities to adjust the sensor direction \cite{geog260}. It is important to consider the temporal resolution in applications that perform change analysis or tracking of temporal events. In such cases, aerial imagery (acquisition of images from camera mounted on aircrafts) is much more suitable since it not constrained by the orbit or the satellite.

    \section{Selection of Satellite Imagery}
        The selection criteria of imagery depend on the needs of the application. The goal of this project is to detect high voltage transmission towers in satellite imagery. Width (width of right of way of a transmission tower \cite{row}) of high voltage transmission towers \cite{towerdesign} can vary from 15m to more than 50m for voltages ranging from 230 kV to 500 kV. Additionally, the structure of a tower is usually composed of steel lattices or trusses, making the structure hollow. Thus, the features of a tower when viewed in a satellite image would be very fine. This would make satellite imagery with high spatial resolution ($<1m$) best suited for this application. Since the position of the towers do not change once constructed, detections would only be performed once, i.e. high temporal resolution imagery is not required for this application. A higher spectral and radiometric metric resolution could be helpful; however, it is not necessary for this application. Additionally, the price to acquire the imagery should be low since the detection of towers would be performed over large areas.

        Keeping in mind the needs and the constraints of this project, the use of satellite imagery from web services has been proposed. High resolution pan-sharpened RGB images are available for public consumption as part of these services. However, the imagery is only available in RGB format, i.e. additional spectrum from multispectral imagery is omitted. The cost for these services is also quite low making them cost effective for large scale detection. Additionally, API's are available to interact with the services making it easier to integrate into the workflow of the application.

    \section{Web Mercator Based Online Services}
        Web Mercator the \cite{epsg:3857,SR-ORG:6864} is the de facto standard for map projections in web applications. It is a variance of the Mercator projection \cite{cs:plane,cs:sphere}. The Cartesian coordinate system used in the Web Mercator projection is designed to meet the conventions used in computer graphics, i.e. $x$ right and $y$ down. Additionally, the poles are projected at infinity, which limits the coverage to 85.051129\textdegree north and south. At this latitude, the map becomes a square. The $x, y$ coordinates are calculated using \cref{eq:webmercator:x,eq:webmercator:y}.
        %
        \begin{equation} \label{eq:webmercator:x}
            x = \frac{128}{\pi} 2^z (\lambda + \pi) \ pixels
        \end{equation}
        \begin{equation} \label{eq:webmercator:y}
            y = \frac{128}{\pi} 2^z \bigg(\pi - \ln \bigg[tan\bigg( \frac{\pi}{4} + \frac{\varphi}{2} \bigg)\bigg]\bigg) \ pixels
        \end{equation}
        %
        where $z$ is the zoom level, $\lambda$ is the longitude in radians and $\varphi$ is the latitude in radians.

        The map at the smallest zoom level ($z=0$) is an $256 \times 256$ image. The total map size can be calculated using \cref{eq:webmercator:wh}. Additionally, the map is divided into a matrix of $2^z \times 2^z$ tiles of $256 \times 256$ pixels each. Table~\ref{table:webmercator:data} shows the characteristics of the map for various zoom levels as measured at the equator.
        %
        \begin{equation} \label{eq:webmercator:wh}
            w = h = 256 \times 2^z
        \end{equation}
        %
        where $w$ is the map width, $h$ is the map height and $z$ is the zoom level.

        Unlike Mercator projection, Web Mercator is not technically conformal \cite{webmercator:nonconformal}. However, the visual difference between Mercator and Web Mercator is minute \cite{webmercator:implications} and the shapes of relatively small objects are preserved. Additionally, north, south, east, west directions are preserved. This allows web applications to display small objects such as buildings, cars etc. Since the primary directions are true at all points in the map and recalculation of projection is not required upon moving the position of the focus, seamless panning and zooming possible \cite{webmercator:mercatornotbad}. These properties make Web Mercator suitable for web based mapping applications despite the scale distortion prevalent in it.

        A few providers offer high resolution satellite imagery as part of their mapping platform. Some of these shall be discussed in the following sections.

        \begin{table}[H]
        \centering
        \resizebox{\textwidth}{!}{%
        \begin{tabular}{@{}llll@{}}
        \toprule
        Zoom level & Map Width and Height (pixels) & Ground Resolution (m/pixel) & Map Scale(at 96 dpi) \\
        \midrule
        1          & 512                           & 78,271.5170                 & 1 : 295,829,355.45   \\
        2          & 1,024                         & 39,135.7585                 & 1 : 147,914,677.73   \\
        3          & 2,048                         & 19,567.8792                 & 1 : 73,957,338.86    \\
        4          & 4,096                         & 9,783.9396                  & 1 : 36,978,669.43    \\
        5          & 8,192                         & 4,891.9698                  & 1 : 18,489,334.72    \\
        6          & 16,384                        & 2,445.9849                  & 1 : 9,244,667.36     \\
        7          & 32,768                        & 1,222.9925                  & 1 : 4,622,333.68     \\
        8          & 65,536                        & 611.4962                    & 1 : 2,311,166.84     \\
        9          & 131,072                       & 305.7481                    & 1 : 1,155,583.42     \\
        10         & 262,144                       & 152.8741                    & 1 : 577,791.71       \\
        11         & 524,288                       & 76.4370                     & 1 : 288,895.85       \\
        12         & 1,048,576                     & 38.2185                     & 1 : 144,447.93       \\
        13         & 2,097,152                     & 19.1093                     & 1 : 72,223.96        \\
        14         & 4,194,304                     & 9.5546                      & 1 : 36,111.98        \\
        15         & 8,388,608                     & 4.7773                      & 1 : 18,055.99        \\
        16         & 16,777,216                    & 2.3887                      & 1 : 9,028.00         \\
        17         & 33,554,432                    & 1.1943                      & 1 : 4,514.00         \\
        18         & 67,108,864                    & 0.5972                      & 1 : 2,257.00         \\
        19         & 134,217,728                   & 0.2986                      & 1 : 1,128.50         \\
        20         & 268,435,456                   & 0.1493                      & 1 : 564.25           \\
        21         & 536,870,912                   & 0.0746                      & 1 : 282.12           \\
        22         & 1,073,741,824                 & 0.0373                      & 1 : 141.06           \\
        23         & 2,147,483,648                 & 0.0187                      & 1 : 70.53            \\
        \bottomrule
        \end{tabular}%
        }
        \caption{Web Mercator map characteristics for various zoom levels. Source: Bing Maps Tile System \cite{msdn:bingmaps}}
        \label{table:webmercator:data}
        \end{table}

        \subsection{DigitalGlobe}
            DigitalGlobe, one of the leading imaging satellite operators, offers satellite imagery as part of their maps API \cite{digitalglobe:mapsapi}. It offers high resolution imagery from a pool of satellites \cite{digitalglobe:satelliteinfo} including WorldView 1-4, GeoEye-1, QuickBird and IKONOS. The maximum zoom level allowed is 18.

            \begin{figure}[H]
            \centering
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/digitalglobe_1}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/digitalglobe_2}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/digitalglobe_3}
            \end{subfigure}
            \caption{Satellite Imagery Samples from Digital Globe}
            \label{fig:digitalglobe}
            \end{figure}

        \subsection{ArcGIS Online}
            ArcGIS offers base layer service comprising of satellite imagery known as ``World Imagery''\cite{arcgis:worldimagery,arcgis:mapserver}. It serves satellite imagery from a variety of sources including 15m TerraColor imagery at small to medium scales, $\sim$0.32m DigitalGlobe Imagery for parts of Western Europe and sub 1m imagery for many parts of the world. The service offers imagery till zoom level 18. Additionally, ArcGIS's service is also metadata enabled. This allows access to the metadata (resolution, collection date and image source) of the best available imagery at a location.

            \begin{figure}[H]
            \centering
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/arcgis_1}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/arcgis_2}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/arcgis_3}
            \end{subfigure}
            \caption{Satellite Imagery Samples from ArcGIS Online}
            \label{fig:argisonline}
            \end{figure}

        \subsection{Google Maps}
            Similar to Arcgis Online, Google Maps \cite{google:mapsapi} offers imagery from various sources including DigitalGlobe's satellites, Landsat etc. It does not provide metadata about the imagery as part of the service. It offers the highest zoom level of up to 20, thus, providing higher spatial resolution.

            \begin{figure}[H]
            \centering
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/google_1}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/google_2}
            \end{subfigure}
            \begin{subfigure}{0.31\textwidth}
                \includegraphics[width=\textwidth]{content/assets/figures/google_3}
            \end{subfigure}
            \caption{Satellite Imagery Samples from Google Maps}
            \label{fig:googlemaps}
            \end{figure}

        \subsection{Selection of Satellite Imagery Source} \label{subsection:satsel}
            The selection criteria depend on the purpose of the imagery in context of this project. Since the goal of the detection of power towers, whose structure would be very fine in satellite imagery. Thus, high spatial resolution is the primary criterion for selection. Out of all three sources mentioned above, Google Maps imagery has been found to be of better quality and of higher spatial resolution (since zoom levels more than 18 are permitted). Imagery from the various sources can be seen in figures \ref{fig:digitalglobe} ,\ref{fig:argisonline} and \ref{fig:googlemaps}




