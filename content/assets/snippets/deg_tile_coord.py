import math

def deg2num(lat, lon, zoom):
    lat_rad = math.radians(lat)
    n = 2.0 ** zoom
    x = (lon + 180.0) / 360.0 * n
    y = (1.0 - math.log(math.tan(lat_rad) + (1 / math.cos(lat_rad))) / math.pi) / 2.0 * n

    xpixel, xtile = math.modf(x)
    ypixel, ytile = math.modf(y)

    return int(xtile), int(ytile), int(xpixel * 256), int(ypixel * 256)

def num2deg(xtile, ytile, xpixel, ypixel, zoom):
    xtile += xpixel / 256.
    ytile += ypixel / 256.

    n = 2.0 ** zoom
    lon = xtile / n * 360.0 - 180.0
    lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * ytile / n)))
    lat = math.degrees(lat_rad)

    return lat, lon
