from utils.dataset.annotations import initialize_sloth_annotations

initialize_sloth_annotations(zoom=19, annotations_file='annotations.json', crop_res=(140., 140.),
                        crop_x_offset=0, crop_y_offset=0, nodes_file='transnet_nodes_germany.csv')
