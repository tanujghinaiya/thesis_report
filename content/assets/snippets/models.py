from models.cnn3 import cnn_regularized

model = cnn_regularized(input_shape=(140, 140, 3), activation_fn='relu', init='glorot_uniform',
                        l1=0.001, l2=0.001, dropout=0.5, weights=None):

from models.hdnn import hdnn

model = hdnn(input_shape=(140, 140, 3), weights=None, activation_fn='relu', init='glorot_uniform',
             l1=0.001, l2=0.001, dropout=0.5):
