germany = [
    'bayern',
    'nordrhein-westfalen',
    'niedersachsen',
    'bremen',
    'sachsen-anhalt',
    'hessen',
    'sachsen',
    'hamburg',
    'saarland',
    'berlin',
    'baden-wuerttemberg',
    'thueringen',
    'rheinland-pfalz',
    'schleswig-holstein',
    'mecklenburg-vorpommern',
    'brandenburg'
]

from utils.parsers.transnet_parser import TransnetParser
from utils.scrapers.osm_nodes_scraper import OsmNodesScraper

powelines_file = 'transnet_powerlines.csv' # File path of the transnet powerlines file
final_tower_data_file = 'transnet_nodes_germany.csv' # File path of the transnet powerlines file
voltages = [380000] # Voltages defined as 380kV

# Initializing the transnet powerlines data parser
transnet_parser = TransnetParser(filename=powelines_file)
transnet_parser.filter_by_regions(regions=germany) # filtering by regions=germany
transnet_parser.filter_by_voltages(voltages=voltages) # filtering by defined voltages, i.e. 380kV

# Initializing the OSM Nodes Scraper by the retrieved nodes and region
osm_scraper = OsmNodesScraper(nodes, region)
n = osm_scraper.scrape(filepath=final_tower_data_file)
# Done. Results saved to transnet_nodes_germany.csv
