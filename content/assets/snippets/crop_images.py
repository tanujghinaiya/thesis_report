import os
from utils.dataset.crop_images import crop_annotated_images

crop_annotated_images(annotations_file='annotations.json',
                      dest_dir=os.path.join('dataset', 'corrected', '19'),
                      negative_samples_per_image=8)