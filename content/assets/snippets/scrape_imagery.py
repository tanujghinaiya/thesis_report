from utils.scrapers.mercator_tiles_scraper import MercatorTilesScraper

zoom = 19
nodes_germany_csv = 'transnet_nodes_germany.csv'

arcgis_scraper = MercatorTilesScraper()
arcgis_scraper.scrape(filename=nodes_germany_csv, zoom=zoom)
arcgis_scraper.affix_tiles(filename=nodes_germany_csv, zoom=zoom)
