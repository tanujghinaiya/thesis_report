import os


def get_path(f):
    return os.path.join(os.getcwd(), os.pardir, f)


def get_fig_path(f):
    return os.path.join(get_path('figures'), f)


def get_data_path(f):
    return os.path.join(get_path('data'), f)
