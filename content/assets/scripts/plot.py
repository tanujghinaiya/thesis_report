import os

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

from utils import *

FIGSIZE = (4.5, 3)

sns.set_palette(sns.color_palette("Set2", 10))

figsize = 10, 6


def plot_nesterov_normal():
    df_normal = pd.read_csv(get_data_path('hyperparam/cnn_140_rgb_lr_0.005000_sgd_he_normal_r_training.log'))
    df_nesterov = pd.read_csv(get_data_path('hyperparam/cnn_140_rgb_lr_0.005000_nesterov_r_training.log'))

    fig, (ax1, ax2) = plt.subplots(1, 2, sharex='all', figsize=figsize)

    ax1.plot(df_normal.loss, label='RGB - Training loss', linewidth=1.5)
    ax1.plot(df_normal.val_loss, label='RGB - Validation loss', linewidth=1.5)

    ax1.plot(df_nesterov.loss, label='G - Training loss', linewidth=1.5)
    ax1.plot(df_nesterov.val_loss, label='G - Validation loss', linewidth=1.5)

    # ax1.set_title('Loss vs Epochs')
    ax1.set_xlabel('Epochs')
    ax1.set_ylabel('Loss')
    ax1.legend(loc='upper right')

    ax2.plot(df_normal.acc, label='RGB - Training accuracy', linewidth=1.5)
    ax2.plot(df_normal.val_acc, label='RGB - Validation accuracy', linewidth=1.5)
    ax2.plot(df_nesterov.acc, label='G - Training accuracy', linewidth=1.5)
    ax2.plot(df_nesterov.val_acc, label='G - Validation accuracy', linewidth=1.5)

    ax2.set_xlabel('epochs')
    ax2.set_ylabel('binary accuracy')
    ax2.legend(loc='upper right')
    #
    # if xlim is not None:
    #     ax1.set_xlim(xlim)
    #     ax2.set_xlim(xlim)
    #
    # if ylim is not None:
    #     ax1.set_ylim(ylim)
    #     ax2.set_ylim(ylim)

    plt.show()


def plot_features():
    df_rgb = pd.read_csv(get_data_path('features/cnn_140_rgb_lr_0.001000_nesterov_r_training.log'))
    df_g = pd.read_csv(get_data_path('features/cnn_140_grey_lr_0.001000_nesterov_r_training.log'))
    df_cl = pd.read_csv(get_data_path('features/cnn_140_edges_dil_ero_lr_0.001000_nesterov_r_training.log'))
    df_hog = pd.read_csv(get_data_path('features/cnn_140_edges_hog_grey_lr_0.001000_nesterov_training.log'))

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=(4.5, 3), dpi=600)

    ax.plot(df_rgb.val_loss, label='RGB', linewidth=1.5)
    ax.plot(df_g.val_loss, label='Grayscale', linewidth=1.5)
    ax.plot(df_cl.val_loss, label='Thresholding + Dilation + Erosion', linewidth=1.5)
    ax.plot(df_hog.val_loss, label='Edges + HOG + Grayscale', linewidth=1.5)

    # ax.set_title('Loss vs Epochs')
    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Loss')
    ax.legend(loc='upper right')

    ax.set_xlim([0, 100])

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('feature_selection_loss.pdf'))

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=(4.5, 3), dpi=600)

    ax.plot(df_rgb.val_acc, label='RGB', linewidth=1.5)
    ax.plot(df_g.val_acc, label='Grayscale', linewidth=1.5)
    ax.plot(df_cl.val_acc, label='Thresholding + Dilation + Erosion', linewidth=1.5)
    ax.plot(df_hog.val_acc, label='Edges + HOG + Grayscale', linewidth=1.5)

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Binary accuracy')
    ax.legend(loc='upper left')

    ax.set_xlim([0, 100])
    ax.set_ylim([0.6, 1])

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('feature_selection_acc.pdf'))
    # plt.show()


def plot_corrections():
    df_raw = pd.read_csv(get_data_path(
        'correction/cnn_140_rgb_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))
    df_corrected = pd.read_csv(get_data_path(
        'correction/cnn_140_rgb_corrected_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))

    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)

    ax.set_xlim([0, 100])

    ax.plot(df_raw.val_loss, label='Raw', linewidth=1.5)
    ax.plot(df_corrected.val_loss, label='Corrected', linewidth=1.5)

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Loss')
    ax.legend(loc='upper right')

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('correction_loss.pdf'))
    # plt.show()

    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)

    ax.set_xlim([0, 100])

    ax.plot(df_raw.val_acc, label='Raw', linewidth=1.5)
    ax.plot(df_corrected.val_acc, label='Corrected', linewidth=1.5)

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Binary Accuracy')
    ax.legend(loc='lower right')

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('correction_acc.pdf'))
    # plt.show()


def plot_training():
    df_cnn3 = pd.read_csv(get_data_path(
        'training/cnn_140_rgb_corrected_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))
    # df_cnn3 = pd.read_csv(get_data_path('training/cnn_140_rgb_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))
    df_hdnn = pd.read_csv(get_data_path(
        'training/hdnn_relu_140_rgb_corrected_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=FIGSIZE, dpi=600)

    ax.plot(df_cnn3.val_loss, label='HDNN', linewidth=1.5)
    ax.plot(df_hdnn.val_loss, label='CNN3', linewidth=1.5)
    # ax.plot(df_cnn3.loss, label='Loss 1')
    # ax.plot(df_hdnn.loss, label='Loss decay')

    # ax.set_title('Loss vs Epochs')
    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Loss')
    ax.legend(loc='upper right')

    ax.set_xlim([0, 100])
    ax.set_ylim([0, 0.75])

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('training_loss.pdf'))
    # plt.show()

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=FIGSIZE, dpi=600)

    # ax.plot(df_cnn3.acc, label='Accuracy 1', linewidth=1.5)
    ax.plot(df_cnn3.val_acc, label='HDNN', linewidth=1.5)
    # ax.plot(df_hdnn.acc, label='Accuracy decay', linewidth=1.5)
    ax.plot(df_hdnn.val_acc, label='CNN3', linewidth=1.5)

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Binary Accuracy')
    ax.legend(loc='upper right')

    # ax.set_ylim([0, 2])
    ax.set_xlim([0, 100])

    # if xlim is not None:
    #     ax.set_xlim(xlim)
    #     ax.set_xlim(xlim)
    #
    # if ylim is not None:
    #     ax.set_ylim(ylim)
    #     ax.set_ylim(ylim)

    plt.tight_layout(0, 0, 0)
    # plt.savefig('hdnn_cnn3.png')
    plt.savefig(get_fig_path('training_acc.pdf'))
    # plt.show()


def plot_finetuning():
    df_cnn3 = pd.read_csv(get_data_path(
        'finetuning/cnn_140_rgb_corrected_full_finetune_decay_lr_0.050000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))
    df_hdnn = pd.read_csv(get_data_path(
        'finetuning/hdnn_relu_140_rgb_corrected_full_lr_0.005000_sgd_he_normal__l1_0.000010_l2_0.000010_dropout_0.500000_r_training.log'))

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=FIGSIZE, dpi=600)

    ax.plot(df_cnn3.val_loss, label='HDNN', linewidth=1.5)
    ax.plot(df_hdnn.val_loss, label='CNN3', linewidth=1.5)
    # ax.plot(df_cnn3.loss, label='Loss 1')
    # ax.plot(df_hdnn.loss, label='Loss decay')

    # ax.set_title('Loss vs Epochs')
    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Loss')
    ax.legend(loc='upper right')

    ax.set_xlim([0, 100])
    # ax.set_ylim([0, 0.75])

    plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('finetuning_loss.pdf'))
    # plt.show()

    fig, ax = plt.subplots(1, 1, sharex='all', figsize=FIGSIZE, dpi=600)

    # ax.plot(df_cnn3.acc, label='Accuracy 1', linewidth=1.5)
    ax.plot(df_cnn3.val_acc, label='HDNN', linewidth=1.5)
    # ax.plot(df_hdnn.acc, label='Accuracy decay', linewidth=1.5)
    ax.plot(df_hdnn.val_acc, label='CNN3', linewidth=1.5)

    ax.set_xlabel('Epochs')
    ax.set_ylabel('Validation Binary Accuracy')
    ax.legend(loc='upper right')

    # ax.set_ylim([0, 2])
    ax.set_xlim([0, 100])

    # if xlim is not None:
    #     ax.set_xlim(xlim)
    #     ax.set_xlim(xlim)
    #
    # if ylim is not None:
    #     ax.set_ylim(ylim)
    #     ax.set_ylim(ylim)

    plt.tight_layout(0, 0, 0)
    # plt.savefig('hdnn_cnn3.png')
    plt.savefig(get_fig_path('finetuning_acc.pdf'))
    # plt.show()


def plot_precision_recall():
    df = pd.read_csv(get_data_path('precision_recall.csv'))
    # fig, ax = plt.subplots(1, 1, figsize=(8, 4))

    g = sns.factorplot(x="name", y="value", hue="label", data=df, kind="bar", aspect=1.5, size=4, palette="muted",
                       legend=False)

    rects = g.ax.patches

    labels = df['value'][:6]

    # print(labels)

    for rect, label in zip(rects, labels):
        h = rect.get_height()
        g.ax.text(rect.get_x() + rect.get_width() / 2, h + 5, label, ha='center', va='bottom')

    g.despine(left=True)

    plt.legend(loc='upper right')
    plt.ylim([0, 1.2])
    plt.xlabel('')
    plt.ylabel('Value')
    plt.savefig(get_fig_path('precision_recall.pdf'))
    # plt.show()

    plt.close('all')

    axis_font = {'fontname': 'Arial', 'size': '15'}

    plt.figure(dpi=600)

    g = sns.factorplot(x="name", y="counts", hue="count_label", data=df, kind="bar", aspect=1.6, size=5,
                       palette="muted", legend=False)

    rects = g.ax.patches

    labels = df['counts']

    for rect, label in zip(rects, labels):
        h = rect.get_height()
        g.ax.text(rect.get_x() + rect.get_width() / 2, h - 10, label, ha='center', va='bottom')

    g.despine(left=True)

    for label in (g.ax.get_xticklabels() + g.ax.get_yticklabels()):
        label.set_fontname('Arial')
        label.set_fontsize(14)

    plt.legend(loc='upper right')
    plt.ylim([0, 3000])
    plt.xlabel('', **axis_font)
    plt.ylabel('Counts', **axis_font)
    plt.savefig(get_fig_path('result_counts.pdf'))
    # plt.show()


def plot_activations():
    from scipy.special import expit

    x = np.arange(-10., 10., 0.2)
    sig = expit(x)
    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)
    ax.set_xlim([-10, 10])
    plt.plot(x, sig)
    # plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('sigmoid.pdf'))
    # plt.show()

    x = np.arange(-10., 10., 0.2)
    tanh = np.tanh(x)
    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)
    ax.set_xlim([-10, 10])
    plt.plot(x, tanh)
    # plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('tanh.pdf'))
    # plt.show()

    x = np.arange(-10., 10., 0.2)
    relu = x * (x > 0)
    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)
    ax.set_xlim([-10, 10])
    plt.plot(x, relu)
    # plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('relu.pdf'))
    # plt.show()

    x = np.arange(-10., 10., 0.2)
    leaky_relu = x * (x > 0) + (.1 * x) * (x < 0)
    fig, ax = plt.subplots(1, 1, figsize=FIGSIZE, dpi=600)
    ax.set_xlim([-10, 10])
    ax.set_ylim([-2, 10])
    plt.plot(x, leaky_relu)
    # plt.tight_layout(0, 0, 0)
    plt.savefig(get_fig_path('leaky_relu.pdf'))
    # plt.show()


def plot_windows():
    from skimage.util.shape import view_as_windows
    from skimage.io import imread

    img = imread(get_fig_path('273767_173941_19.jpg'))
    windows = view_as_windows(img, (140, 140, 3), 80)

    fig, ax = plt.subplots(windows.shape[0], windows.shape[1], sharex='all', sharey='all', figsize=(10, 10))

    for i in range(windows.shape[0]):
        for j in range(windows.shape[1]):
            ax[i, j].axis('off')
            ax[i, j].set_aspect('equal')
            ax[i, j].imshow(windows[i, j, 0])
            # print(i, j)

    print(windows.shape)

    # fig.subplots_adjust(wspace=0, hspace=0)
    # fig.tight_layout()
    plt.tight_layout(pad=0, w_pad=0, h_pad=0)
    plt.margins(0, 0)
    plt.savefig(get_fig_path('273767_173941_19_windows.jpg'), bbox_inches='tight', pad_inches=0)
    # plt.show()


if __name__ == '__main__':
    # plot_features()
    # plot_corrections()
    # plot_training()
    # plot_finetuning()
    plot_precision_recall()
    # plot_activations()
    # plot_windows()
